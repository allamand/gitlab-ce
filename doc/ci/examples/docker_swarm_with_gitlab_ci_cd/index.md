# Using GitLab for CI/CD on Docker-swarm

## Introduction

GitLab offers very powerful means to create [Continuous Integration & Deployment
Pipelines](https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/)
for each of your product/projects. The Goal is to have code frequently pushed,
built, tested and deployed, in an automated way you previously defined.
You can find more information on GitLab [CI docs](/ee/ci/README.html)

With this step-by-step tutorial I will show you how to uses GitLab.com to :

- Build and test a microservice application
- Create on the fly a Docker swarm cluster on (but you could easilly adapt to
  your swarm cluster.)
- Deploy the application on the Docker swarm.

To be able to demonstrate the automatic deployment of the Docker voting app to a
[Play with Docker](http://play-with-docker.com) Swarm cluster, I've set up this
[sample project](https://gitlab.com/allamand/example-voting-app).

> You need a GitLab.com account to fork this project and follow this tutorial.

## Step 1: Create a GitLab project for your application

### Project description

Since we deploy our application on a [Docker Swarm
Mode](https://docs.docker.com/engine/swarm/) cluster, we will make use of
[Docker overlay
network](https://docs.docker.com/engine/userguide/networking/get-started-overlay/#overlay-networking-and-swarm-mode)
and create a `public` and `private` network. Our application will have 2
frontend services that must be exposes on the Internet, so we set-up a double
network attachment on public and private network. The backends services will not
be exposed on Internet and will only be attached on the private network. The
[Traefik](https://traefik.io/) Reverse Proxy role is to dispatch our requests
from internet to the frontend services and will be listening only on the public
Network. Only Traefik will exposes ports on the swarm.

This microservices application can be describe as :

- A Docker Swarm **public network** name `traefik_net` to expose our frontend
  services :
    - a **Traefik** reverse Proxy which is the public endpoint for the 2
      frontend services:
    - a **vote** **Python** web-app, which lets you vote between two options
    - a **result** **Node.js** web-app which shows the results of the voting in
      real-time.
- A Docker Swarm **private network** name `private` which links our frontend to
  our backend micro services :
    - a **Redis** queue which collects new votes
    - a **worker** **.NET** service which consumes votes, and store them in..
    - a **Postgres** database, backed by a Docker Volume

![Application architecture](img/gitlab_vote_traefik_archi.png)

>If you want more information on using Docker swarm and configuring Traefik you
>can follow my [Traefik tutorial on play-with-docker
>labs](http://training.play-with-docker.com/traefik-load-balancing/)

## Step 2: Manually deploy the application

For better understanding what we are going to automate with GitLab, we first
deploy manually the application.

### Step 2.1: Create a Docker swarm cluster on Play-with-Docker

In order to allow everyone to follow (without the pre-requisite to already have
a Docker Swarm cluster), we'll make usage of the great [Play With
Docker](http://play-with-docker.com) platform, which allow us to create in
seconds public Docker swarm clusters. Please, create a session on Play With
Docker. You'll need a valid Docker account to access the platform.

![PWD interface screen](img/gitlab_pwd_interface.png)

Click **ADD NEW INSTANCE** in order to create an instance

Create your swarm cluster by typing the following command :

```bash
docker swarm init --advertise-addr eth0
```

Checkout the example repository, or your fork:

```bash
git clone https://gitlab.com/allamand/example-voting-app.git
cd example-voting-app
```

### Step 2.2: Deploy Traefik reverse proxy on Docker swarm

The first thing we will need to do, is to deploy the Traefik Reverse Proxy. We
are using [Docker Stack](https://docs.docker.com/engine/swarm/stack-deploy/) to
deploy our applications and [docker-compose
v3](https://docs.docker.com/compose/compose-file/) file in order to describe the
stack to deploy.

You can found the stack for Traefik in the
[traefik.yml](https://gitlab.com/allamand/example-voting-app/blob/master/traefik.yml)
docker-compose file.

```yaml
version: '3'

services:

  traefik:
    image: traefik:1.3.4
    command: -l warning --docker --docker.swarmmode --docker.domain=docker.localhost --docker.watch --web
    ports:
      - '80:80'
      - '8080:8080'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - net
    deploy:
      replicas: 1
      placement:
        constraints: [node.role == manager]
      restart_policy:
        condition: on-failure
      labels:
        traefik.enable: "false"

networks:
  net:
    driver: overlay
```

With this compose file we define the Traefik service that we want to deploy. It
will be using `docker.swarmmode`, and expose port 80 for incoming application
request and port 8080 for Traefik Dashboard overview. Traefik needs to
communicate with a Docker swarm manager, for that we mount the Docker socket in
the container, and we add a deployment constraint so that the service will be
deployed on a Docker swarm manager node. In this file we also create a network
for this service names `net` which will have an external name of `<stack>_net :
traefik_net`.

To create the Traefik stack (which will deploy this service and create the
network) on the swarm you need to execute:

```bash
docker stack deploy traefik -c traefik.yml
```

we can check the status of the deployment using :

```bash
docker stack ls
docker stack ps traefik
```

>**Note:** Once the deployment is OK, because we are exposing 2 ports on the
Swarm, PWD will update the session screen with 2 links:
- `8080` link to access the Traefik dashboard
- `80` link to access exposed services (at this time there is not yet services
  exposed)

![PWD interface exposing ports](img/gitlab_pwd_interface_ports.png)

### Step 2.3: Build & deploy our application on Docker swarm

We will uses similar approach to build and deploy the voting application using
the
[docker-compose-traefik.yml](https://gitlab.com/allamand/example-voting-app/blob/master/docker-compose-traefik.yml)
file. Here is an extract of this file defining the vote service:

```yaml
services:

  vote:
    build: ./vote
    image: ${REGISTRY_SLASH-sebmoule/}vote_vote${COLON_TAG-:latest}
    command: python app.py
    depends_on:
      - redis
    deploy:
      replicas: 1
      labels:
        - "traefik.backend=vote"
        - "traefik.port=80"
        - "traefik.frontend.rule=PathPrefixStrip:/vote"
        - "traefik.docker.network=traefik_net"
    networks:
     - traefik_net
     - private

...
```

- **build** defines the path where to find the `Dockerfile` in order to build
  the vote service
- **image** is the image name to build, or to use to run the container.
    - We uses the environment variables `REGISTRY_SLASH` and `COLON_TAG` in
      order to be able to change image name and tag we are going to build.
	- if not provided it will uses the default values
      `sebmoule/vote_vote:latest`

> **Note:** `sebmoule` is my Docker Hub account ;)

First we need to build the application 
```bash
docker-compose build
```

You may be able to see the builded images with `docker images`

```bash
[node1] (local) root@10.0.35.3 ~/example-voting-app
$ docker images
REPOSITORY             TAG                 IMAGE ID            CREATED              SIZE
sebmoule/vote_result   latest              7fdb6475d896        40 seconds ago       229MB
sebmoule/vote_vote     latest              7e06d330091f        About a minute ago   84.7MB
sebmoule/vote_worker   latest              6d6083af8cd6        About a minute ago   635MB
traefik                <none>              dbcae52bfdec        12 days ago          45MB
python                 2.7-alpine          35084243e776        3 weeks ago          72MB
microsoft/dotnet       1.0.0-preview1      5be9548822f7        10 months ago        576MB
node                   5.11.0-slim         cb888ea932ad        15 months ago        207MB
```

- **labels** allows us to define our container parameters to the Traefik proxy

```yaml
      labels:
        - "traefik.backend=vote"
        - "traefik.port=80"
        - "traefik.frontend.rule=PathPrefixStrip:/vote"
        - "traefik.docker.network=traefik_net"
```

Those labels, will be attached to the containers deployed into the swarm cluster
and will be dynamically read by Traefik so that it can reconfigure itself with
according instructions: In this example, it tells Traefik to proxify every
request coming with the `/vote` URI to the backend, using the `traefik_net`
Docker network. Thanks to Docker swarm mode internal DNS feature, the Traefik
container will be able to contact the vote containers just by calling `vote:80`.

- **networks** - this container has a double overlay network attachment
    - one to the `traefik_net` network previously created with the `traefik`
      stack, which is our public network
	- the other is the `private` network which will be used for backend
      communications.

To deploy the Application Stack :

```bash
docker stack deploy vote -c docker-compose.yml
```

we can see the status of the deployment using :

```bash
docker stack ls
docker stack ps vote
```

The stack deployment may take a few time to finalize while Docker needs to
download missing Docker images and waits for all containers to be in the
**Running** **Desired state**

```bash
ID                  NAME                IMAGE                         NODE                DESIRED STATE       CURRENT STATE              ERROR               PORTS
lbux68xct1o4        vote_worker.1       sebmoule/vote_worker:latest   node1               Running             Preparing 9 seconds ago
f9c7hsvbzk40        vote_redis.1        redis:alpine                  node1               Running             Preparing 9 seconds ago
ikjz83k386hs        vote_vote.1         sebmoule/vote_vote:latest     node1               Running             Preparing 10 seconds ago
nybaxgy3ol9p        vote_result.1       sebmoule/vote_result:latest   node1               Running             Preparing 10 seconds ago
dkn3er3uscsr        vote_db.1           postgres:9.4                  node1               Running             Preparing 11 seconds ago
```

To access to our service, click on the `80` link in the PWD UI, and add `/vote`
at the end of the url this would be something like :

http://pwd10-0-35-3-80.host2.labs.play-with-docker.com/vote

you should visualize the application :

![Vote service](img/gitlab_pwd_cats.png)

make your vote, and switch to the `/result` endpoint to see your vote!!

![Result service](img/gitlab_vote_result.png)

In this step, we have see how to successfully deploy our application on a Docker
swarm cluster created on Play-with-Docker. Now we are going to automate this
with GitLab CI.

## Step 3: Automate deployment using GitLab ci pipeline

We want that each time we push some code into our repository it redeploy our
application on the swarm cluster.

we will use [GitLab Docker
registry](https://about.gitlab.com/2016/05/23/gitlab-container-registry/) to
store the Docker images we build from our source.

### Step 3.1: Fork the application

If you haven't already done, please fork [my demo
project](https://gitlab.com/allamand/example-voting-app) on your gitlab account:

### Step 3.2: Configure GitLab with Play-With-Docker url

This tutorial shows you how to automate and deploy a microservice application on
a Docker Swarm Cluster. In order to do this we needs our GitLab Runner to be
able to talk to the Swarm Cluster. Docker Provide an easy way to configure a
Docker client to talk to a remote Docker Daemon. You could easilly adapt this to
allow your gitlab runner to talk to your on premise swarm cluster.

> We uses the DOCKER_HOST Variable to allow Docker client to talk to the PWD
> docker deamon

Please, create a new fresh session on [Play With
Docker](http://play-with-docker.com** platform. Play With Docker Introduce a new
**SSH** Button that copies a direct ssh link to your PWD Instance. This is
something like :

```bash
ssh ip172-18-0-26-b92h98gl9m7g009a91ig@direct.labs.play-with-docker.com
```
This allows also to talk to our PWD instance Docker daemon with small adaptation: 

- replace the `@` with a `.`
- add preffix it with ``tcp://

the pipeline will create this environment variable looking like this :

```bash
export DOCKER_HOST=tcp://ip172-18-0-26-b92h98gl9m7g009a91ig.direct.labs.play-with-docker.com
```

Store the value as a GitLab Pipeline variable. In GitLab, go to
**Settings/Pipelines** and scroll down to add a new `DOCKER_HOST_VAR` with value
`tcp://ip172-18-0-26-b92h98gl9m7g009a91ig.direct.labs.play-with-docker.com`

![GitLab Secret variables](img/gitlab_secret_var.png)

Then click `Add new variable` button.

### Step 3.3: Trigger your first pipeline

Now, in order to trigger a new build, you need to commit a change in a file, or
just create a new branch.

![GitLab Pipeline](img/gitlab_pipeline.png)

The Pipeline is composed of 4 steps:

- **check**: This job will connect to your PWD Instance and make a `docker
  info`. This is to check that your PWD instance is quitely configured and up &
  running, since the PWD instance had only 4 hours lifetime.
- **Build** : build services Docker images and to push themes in the [GitLab
  Registry](https://gitlab.com/allamand/example-voting-app/container_registry)
- **Test** : instantiate our application within a GitLab runner instance, in
  order to execute functional tests
- **Deploy** if the Tests are OK, we create a Docker swarm on Play-with-docker
and deploy our application on it. <!--- **Doc** : Finally the Doc job will
simply deploy the documentation of our project to [GitLab
Page](https://docs.gitlab.com/ce/user/project/pages/index.html). -->

>while the pipeline is running let's walk through the yaml file that describe it

### Step 3.4: Explaining the pipeline

You can find the pipeline definition in [.gitlab-ci.yml file
here](https://gitlab.com/allamand/example-voting-app/blob/master/.gitlab-ci.yml)
and let's break it to explain each part:

#### Image and services

```yaml
image: registry.gitlab.com/build-images/docker:latest

services:
  - docker:dind
```

- The **image** define which image to uses for the jobs. I use a build image I
  have define in this [GitLab project](https://gitlab.com/build-images/docker),
  which contains proper Docker tools.
- The **services** defines additional Docker images which are linked to the main
  container. In this example our main image will be linked to a Docker in Docker
  images which will allow us to use the Docker daemon of the linked dind image
  using the environment variable `DOCKER_HOST='tcp://docker:2375'`. You can find
  more information in the
  [documentation](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#use-docker-in-docker-executor).

#### Stages

```yaml
stages:
  - build
  - test
  - deploy
  - dpc
```

Under the stage command we will define the order of the Jobs. Each Jobs related
to the same stage will be executed in parallel, while stages are triggered
sequentially using the order defined. We can define as many stages as we need
for our workflow

#### Job1: Build and push to registry

```yaml
Build & Push to Registry:
  stage: build
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"
  script:
    - echo "Using GitLab registry $REGISTRY_SLASH and $COLON_TAG"
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH        
    - docker-compose -f docker-compose-gitlab.yml build
    - docker-compose -f docker-compose-gitlab.yml push
  except:
    - tags
  tags:
    - docker
    - pwd
```

- The **Build & Push to Registry** job goal is to build our services images using
  our `docker-compose-gitlab.yml`. Each Job is related to a stage so that GitLab
  knows in which order execute themes.
- The `CI_REGISTRY_IMAGE` is a specific GitLab variable containing the Registry
  address of our project.

>**Note:** For a complete list of pre-defined GitLab variable see the [variables
documentation](https://docs.gitlab.com/ce/ci/variables/README.html#variables)

In this step, we ask the runner to authenticate to our project GitLab registry
(`docker login`) build our application's services Docker images (`docker-compose
build`) and store themes in the GitLab Registry (`docker-compose push`).

You can see the `vote_vote` and the `vote_result` Docker images by clicking on
the **Registry** link ([Example for my
project](https://gitlab.com/allamand/example-voting-app/container_registry))

![GitLab Vote registry](img/gitlab_vote_registry.png)

---

#### Job2: Pull, deploy & test locally

```yaml
Pull & deploy & test locally:
  stage: test
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"      
  script:
    - echo "Using gitlab registry $REGISTRY_SLASH and $COLON_TAG"    
    - docker swarm init || true # if already a swarm
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH    
    - docker-compose -f docker-compose-gitlab-injob.yml pull
    - docker stack deploy vote -c docker-compose-gitlab-injob.yml
    - sleep 5
#YOU CAN MAKE YOUR TESTS HERE
    - docker-compose -p test-vote -f docker-compose-tests.yml run newman
    - docker-compose -p test-vote -f docker-compose-tests.yml down
    - docker stack rm vote
  artifacts:
    paths:
      - tests/newman
  except:
    - tags

```

We uses a GitLab runner in a `test` stage in order to deploy the application in
a sandbox environment and execute functional tests with it.

I usually uses docker-compose with specific testing image to validate an API or
else. In this example I uses a test container based on
[newman](https://github.com/postmanlabs/newman-docker) to execute tests
scenarios I have previously create using [Postman](https://www.getpostman.com/)
and stored within the Git repository test folder.

!!! warning "At this stage, the tutorial focus on deploy on swarm, and I didn't
set up tests for the application yet ;)"

#### Job3: Deploy to swarm

If the tests are OK, I want GitLab to proceed with the next step which is the
deployment of my application on Docker swarm

```yaml
Deploy to Swarm:
  stage: deploy
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"
  before_script:
	#You need to set-up PWD_URL in the Settings of your GitLab Pipeline variable
    - docker-machine create -d pwd node1
    - eval `docker-machine env node1 --shell bash`
    - docker swarm init --advertise-addr eth0 || true
    - docker stack deploy -c traefik.yml traefik
    - docker stack ls
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH
    - docker-compose -f docker-compose-gitlab.yml pull
    - docker stack deploy vote --with-registry-auth -c docker-compose-gitlab.yml
  only:
    - master
  except:
    - tags
```

- The Job retrieve the `$PWD_URL` from our GitLab Variables we defined earlier
  in the `Settings/Pipeline` section
- Then we uses `docker-machine create -d pwd node1` to create the Docker VM on
  Play-With-Docker

!!! tip "If you have a Docker Swarm of your own, it's easy to change this step
just to configure the DOCKER_HOST variable to point to your cluster instead."

- `docker-machine env node1` is used to configure the local Docker client to
  talk to our fresh Docker daemon
- We create the swarm cluster with the `docker swarm init` command and we deploy
  the application with `docker stack deploy` first for the **Traefik** proxy and
  then for the **vote** application.

!!! tips "We also could have add here some validation tests to be sure our
deployment is good to go."

### Step 3.5 Validating the deployment

After few minutes, you can see that an instance must have pop-up in your PWD
Interface, ports 80 and 8080 are now exposed (as when we done it manually). If
you click on the **80** link you will get the Docker swarm node visualizer
service. If you add the `/vote` and `/result` endpoints you should see that the
voting application has been deployed automatically.

![node visualizer](img/node_visualizer.png)

If you click on the **8080** port you should be the Traefik dashboard with our 2
exposed services:

![Traefik dashboard](img/traefik_vote_dashboad.png)

You can found here the whole `.gitlab-ci.yml` file :

```yaml
# When using dind, it's wise to use the overlayfs driver for
# improved performance.
variables:
  DOCKER_DRIVER: overlay
  REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
  COLON_TAG: ":1.0.0"

image: registry.gitlab.com/build-images/docker:latest

services:
  - docker:dind

stages:
  - check
  - build
  - test
  - deploy
  - stop

variables:
  dockerfile_dir: "$CI_PROJECT_DIR"

Check PWD:
  stage: check
  script:
    - export DOCKER_HOST=$DOCKER_HOST_VAR
    - echo using DOCKER_HOST=$DOCKER_HOST
    - docker info

Build & Push to Registry:
  stage: build
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"
  script:
    - echo "Using gitlab registry $REGISTRY_SLASH and $COLON_TAG"
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH
    - docker-compose -f docker-compose-gitlab.yml build
    - docker-compose -f docker-compose-gitlab.yml push
  except:
    - tags

Pull & deploy & test locally:
  stage: test
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"
  script:
    - echo "Using gitlab registry $REGISTRY_SLASH and $COLON_TAG"
    - docker version
    - docker swarm init || true # if already a swarm
    - docker ps -a
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH
    - docker swarm init --advertise-addr eth0 || true
    - docker stack deploy -c traefik.yml traefik
    - docker-compose -f docker-stack-traefik.yml pull
    - docker stack deploy vote -c docker-stack-traefik.yml
    - sleep 5
    - docker-compose -p test-vote -f docker-compose-tests.yml run newman
    - docker stack rm vote
  artifacts:
    paths:
      - tests/newman
  except:
    - tags

Deploy to Swarm:
  stage: deploy
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"
  before_script:
    - export SHELL=/bin/bash
    - echo "Creation of Docker Swarm on PWD"
    #You need to set-up DOCKER_HOST_VAR in the Settings of your GitLab Pipeline in secret variable
    - export DOCKER_HOST=$DOCKER_HOST_VAR
    - echo using DOCKER_HOST=$DOCKER_HOST
    - docker swarm init --advertise-addr eth0 || true
    - docker stack deploy -c traefik.yml traefik
    - docker stack ls
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH
    - docker stack deploy vote --with-registry-auth -c docker-stack-traefik.yml
    #YOU CAN EXECUTE POST INSTALL TESTS HERE
```

## Conclusion

In this tutorial we have seen how it can be very easy to combined GitLab with a
Docker Swarm Cluster to make a great [Continuous
Delivery](https://en.wikipedia.org/wiki/Continuous_delivery) pipeline, automated
by a simple `git push` command from your source code.

Of course the uses of Play-with-docker for this tutorial makes this project not
viable for real life and you should uses your own real Docker swarm clusters.

## To go further

### Make use of GitLab environments

An easy upgrade of this Pipeline would be to allow to create severals
deployments environments, one for each git branch. This is available through the
uses of [GitLab Environments](https://docs.gitlab.com/ce/ci/environments.html),
just adding some specific line in the .gitlab-ci.yml file!!

>**Note:** Since with Play-with-docker we can't predict in advance the url of
the service I don't have used themes in this tutorial.

## TroubleShoot

### Error in deployment pipeline

If you got the following error in your Pipeline

```bash
Creating machine...
Error creating machine: Error in driver during machine creation: Could not create instance Post http://host2.labs.play-with-docker.com:80/sessions/37e71e63-438e-4fe2-85e0-71cf5348b302/instances: dial tcp 34.206.199.2:80: i/o timeout <nil>
ERROR: Job failed: exit code 1
```

It is because your PWD_URL link is out-of-date. In this case, create a new [Play
With Docker](http://play-with-docker.com) session, store the new URL in your
**Setting/Pipelines/Secret variables** section then relaunch the deployment Job

>**Note:** Remember that the PWD_URL link is only valid for 4 hours
